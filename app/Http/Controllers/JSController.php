<?php

namespace App\Http\Controllers;

use App\Viagem;
use Illuminate\Http\Request;

class JSController extends Controller
{
    public function index()
    {
        //$viagens = Viagem::orderBy('id')->get();
        $viagens = Viagem::select('id as numeroviagem','veiculo', 'codatendimento', 'motorista', 
        'cobrador', 'inicioprogramado', 'iniciorealizado', 'fimprogramado','fimrealizado', 'data', 'codpontoinicio',
        'nomepontoinicio', 'codpontofim', 'nomepontofim','atividade', 'tabela', 'tipodia', 
        'toleranciaatrasosaida','toleranciaadiantamentosaida','toleranciaatrasochegada', 'toleranciaadiantamentochegada',
        'toleranciatempoviagem','eventos')->get();

        return response()
            ->json( ['viagens'=> $viagens] , 200, [], JSON_PRETTY_PRINT);
    }
    public function store(Request $request)
    {
        $dados = $request->all();
        $inc = Viagem::create($dados);
        if ($inc) {
            return response()->json([$inc], 201);
        } else {
            return response()->json(['error' => 'error_insert'], 500);
        }
    }
    public function show($id)
    {
        $reg = Viagem::find($id);
        if ($reg) {
            return response()
                ->json($reg, 200, [], JSON_PRETTY_PRINT);
        } else {
            return response()
                ->json(['error' => 'not_found'], 404);
        }
    }
    public function update(Request $request, $id)
    {
        $reg = Viagem::find($id);
        if ($reg) {
            $dados = $request->all();
            $alt = $reg->update($dados);
            if ($alt) {
                return response()
                    ->json($reg, 200, [], JSON_PRETTY_PRINT);
            } else {
                return response()
                    ->json(['error' => 'not_update'], 500);
            }
        } else {
            return response()
                ->json(['error' => 'not_found'], 404);
        }
    }
    public function destroy($id)
    {
        $reg = Viagem::find($id);
        if ($reg) {
            if ($reg->delete()) {
                return response()
                    ->json(['msg' => 'Ok! Excluído'], 200);
            } else {
                return response()
                    ->json(['error' => 'not_destroy'], 500);
            }
        } else {
            return response()
                ->json(['error' => 'not_found'], 404);
        }
    }
}
