<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Viagem extends Model
{
    //
    protected $table = 'viagens';
    protected $primaryKey = 'id';
    protected $fillable = [ 'users_id', 'veiculo', 'codatendimento', 'motorista', 
    'cobrador', 'inicioprogramado', 'iniciorealizado', 'fimprogramado','fimrealizado', 'data', 'codpontoinicio',
    'nomepontoinicio', 'codpontofim', 'nomepontofim','atividade', 'tabela', 'tipodia', 
    'toleranciaatrasosaida','toleranciaadiantamentosaida','toleranciaatrasochegada', 'toleranciaadiantamentochegada',
    'toleranciatempoviagem','eventos'];
    

    public function users() {
        return $this->belongsTo('App\User');
    }
}
