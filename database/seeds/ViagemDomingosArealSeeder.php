<?php

use Illuminate\Database\Seeder;

class ViagemDomingosArealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       //tabela 101M
       DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'ARI',
        'cobrador' => 'RICARDO',
        'inicioprogramado' => '05:30',         
        'fimprogramado' => '06:00',         
        'data' => '03/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'ARI',
        'cobrador' => 'RICARDO',
        'inicioprogramado' => '06:00',         
        'fimprogramado' => '06:25',         
        'data' => '03/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
   
   
  
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => 'ARI',
        'cobrador' => 'RICARDO',
        'inicioprogramado' => '10:00',         
        'fimprogramado' => '10:30',
        'data' => '03/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => 'ARI',
        'cobrador' => 'RICARDO',
        'inicioprogramado' => '10:30', 
        'fimprogramado' => '10:55',        
        'data' => '03/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
  
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1001',
        'motorista' => 'ARI',
        'cobrador' => 'RICARDO',
        'inicioprogramado' => '12:00',         
        'fimprogramado' => '12:20',
        'data' => '03/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1001',
        'motorista' => 'ARI',
        'cobrador' => 'RICARDO',
        'inicioprogramado' => '12:20',         
        'fimprogramado' => '12:55',
        'data' => '03/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
   
  //tabela 101T
  DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1000',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '14:00',         
    'fimprogramado' => '14:30',
    'data' => '03/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1000',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '14:30',         
    'fimprogramado' => '14:55',
    'data' => '03/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);


DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1002',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '19:00',         
    'fimprogramado' => '19:30',
    'data' => '03/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1002',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '19:30',         
    'fimprogramado' => '20:55',
    'data' => '03/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1000',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '20:00',         
    'fimprogramado' => '20:30',
    'data' => '03/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1000',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '20:30',         
    'fimprogramado' => '20:55',
    'data' => '03/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);

DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1000',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '22:00',         
    'fimprogramado' => '22:30',
    'data' => '03/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '101',
    'codatendimento' => '1000',
    'motorista' => 'PEDRO',
    'cobrador' => 'ANGELO',
    'inicioprogramado' => '22:30',         
    'fimprogramado' => '23:00',
    'data' => '03/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '101T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
    }
}
