<?php

use Illuminate\Database\Seeder;

class ViagemDomingosFragataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //tabela 201M
      DB::table('viagens')->insert([
        'veiculo' => '201',
        'codatendimento' => '1020',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
        'inicioprogramado' => '05:30',         
        'fimprogramado' => '06:00',         
        'data' => '03/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '201M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '201',
        'codatendimento' => '1020',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
        'inicioprogramado' => '06:00',         
        'fimprogramado' => '06:25',         
        'data' => '03/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '201M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
   
    DB::table('viagens')->insert([
        'veiculo' => '201',
        'codatendimento' => '1020',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
        'inicioprogramado' => '07:30',         
        'fimprogramado' => '08:00',
        'data' => '03/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '201M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '201',
        'codatendimento' => '1020',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
        'inicioprogramado' => '08:00',         
        'fimprogramado' => '08:30',
        'data' => '03/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '201M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
   
   
    DB::table('viagens')->insert([
        'veiculo' => '201',
        'codatendimento' => '1020',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
        'inicioprogramado' => '11:00',         
        'fimprogramado' => '11:30',
        'data' => '03/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '201M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    
    DB::table('viagens')->insert([
        'veiculo' => '201',
        'codatendimento' => '1020',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
        'inicioprogramado' => '12:20',         
        'fimprogramado' => '12:55',
        'data' => '03/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '201M',
        'tipodia' => 'DOMINGOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
 
  //tabela 201T
 
DB::table('viagens')->insert([
    'veiculo' => '201',
        'codatendimento' => '1021',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
    'inicioprogramado' => '15:30',         
    'fimprogramado' => '15:50',
    'data' => '03/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '201T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '201',
    'codatendimento' => '1021',
    'motorista' => 'JOAO',
    'cobrador' => 'LUIS',
    'inicioprogramado' => '15:50',         
    'fimprogramado' => '16:25',
    'data' => '03/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '201T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);



DB::table('viagens')->insert([
    'veiculo' => '201',
        'codatendimento' => '1020',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
    'inicioprogramado' => '20:00',         
    'fimprogramado' => '20:30',
    'data' => '03/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '201T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '201',
    'codatendimento' => '1020',
    'motorista' => 'JOAO',
    'cobrador' => 'LUIS',
    'inicioprogramado' => '20:30',         
    'fimprogramado' => '20:55',
    'data' => '03/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '201T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '201',
    'codatendimento' => '1021',
    'motorista' => 'JOAO',
    'cobrador' => 'LUIS',
    'inicioprogramado' => '21:00',         
    'fimprogramado' => '21:20',
    'data' => '03/11/2020',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '201T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '201',
        'codatendimento' => '1021',
        'motorista' => 'JOAO',
        'cobrador' => 'LUIS',
    'inicioprogramado' => '21:20',         
    'fimprogramado' => '21:55',
    'data' => '03/11/2020',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '201T',
    'tipodia' => 'DOMINGOS',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);

    }
}
