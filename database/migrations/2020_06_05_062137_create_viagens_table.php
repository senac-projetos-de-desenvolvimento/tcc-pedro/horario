<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateViagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viagens', function (Blueprint $table) {
                //migração da tabela viagens
                $table->bigIncrements('id');
                $table->unsignedBigInteger('users_id')->nullable();
                $table->string('veiculo',100)->nullable();
                $table->string('codatendimento',100)->nullable();
                $table->string('motorista',100)->nullable();
                $table->string('cobrador',100)->nullable();
               $table->string('inicioprogramado',100)->nullable();
                $table->string('iniciorealizado',100)->nullable();
                $table->string('fimprogramado',100)->nullable();
                $table->string('fimrealizado',100)->nullable();
                $table->string('data',100)->nullable();
                $table->string('codpontoinicio',100)->nullable();
                $table->string('nomepontoinicio',100)->nullable();
                $table->string('codpontofim',100)->nullable();
                $table->string('nomepontofim',100)->nullable();
                $table->string('atividade',100)->nullable();
                $table->string('tabela',100)->nullable();
                $table->string('tipodia',100)->nullable();
                $table->string('toleranciaatrasosaida',100)->nullable();
                $table->string('toleranciaadiantamentosaida',100)->nullable();
                $table->string('toleranciaatrasochegada',100)->nullable();
                $table->string('toleranciaadiantamentochegada',100)->nullable();
                $table->string('toleranciatempoviagem',100)->nullable();
                $table->string('eventos',100)->nullable();
                $table->timestamps();
               $table->foreign('users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viagens');
    }
}
